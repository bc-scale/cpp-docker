FROM alpine:latest

# RUN apk update; apk upgrade;
RUN apk add --no-cache git cmake clang clang-dev make gcc g++ libc-dev linux-headers

COPY . /opt/cpp-docker

RUN mkdir /opt/cpp-docker/build
WORKDIR /opt/cpp-docker/build

RUN cmake ..
RUN make

RUN mv bin/cpp-docker /usr/bin/cpp-docker

# EXPOSE 3000
WORKDIR /usr/bin
ENTRYPOINT ["./cpp-docker"]


# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="CPP_DOCKER" \
      org.label-schema.description="A Simple C++ Dockerized Application" \
      org.label-schema.url="https://jscheer.de" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://jscheer.de" \
      org.label-schema.vendor="Jonas Scheer" \
      org.label-schema.version="deployment" \
      org.label-schema.schema-version="1.0"

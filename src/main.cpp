#include <string>
#include <thread>
#include <ctime>
#include <chrono>
#include <iostream>
#include <fstream>

using namespace std::chrono_literals;

int main(int argc, char *argv[]) {

  printf("A simple Example Application \n");

  int sleep_ms = 1000;
  if(argc > 1) {
    sleep_ms = atoi(argv[1]);
  }

  int count = -1;
  if(argc > 2) {
    count = atoi(argv[2]) - 1;
  }

  std::ofstream file_out;
  std::string filename("");
  if(argc > 3) {
    filename = argv[3];
    file_out.open(filename, std::ios_base::app);
    if (file_out.is_open()) {
      file_out << "\nStart File: " << std::endl;
    }
    else {
      printf("Can not open file:\t %s \n", filename.c_str());
    }
  }

  while(true) {

    // write time to file:
    if (file_out.is_open()){
      std::time_t now_time = std::chrono::system_clock::to_time_t(
                           std::chrono::system_clock::now());
      file_out << "time: " << std::ctime(&now_time) << std::endl;
    }
    else {
      printf("Can not open file:\t %s \n", filename.c_str());
    }

    // print console output
    printf("Remainint Iterations: %d\t (sleep: %d)\n", count+1, sleep_ms);
    printf("\t(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)\n\n");
    std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));

    // update counter
    if(count == 0) {
      break;
    }
    else {
      if(count > 0) {
        count--;
      }
    }
  }

  if (file_out.is_open()){
    file_out << "End File. " << std::endl;
    file_out.close();
  }

  return 0;
}

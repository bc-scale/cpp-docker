# cpp-docker

# Build Docker Image
```bash
docker build -t scheerjo/cpp-docker:0.0.1 .
```

## Specify Platform Architecture
When building on OSX (arm) but deplyong for amd64, we can build with:
``` bash
docker build --no-cache --platform=linux/amd64 -t scheerjo/cpp-docker:0.0.1 .
```

## Publush Docker
We can push the built image using:
``` bash
docker push scheerjo/cpp-docker:0.0.1
```

### Docker Hub Login
On Dockerhub, we must create an `Access Token`. <br>
Afterwards we can use this token as password and login by entering: `docker login -u scheerjo`

### Pull from Docker Hub
``` bash
docker pull scheerjo/cpp-docker:0.0.1
```

## Run Docker
run with:
```bash
docker run -it scheerjo/cpp-docker:0.0.1 <delay_ms> <loop_count>
```
- `<delay_ms>`: delay in milliseconds between each iteration
- `<loop_count>`: amount of iterations

Note: Without any arguments, there will be an infinite loop.

### Example Output
We get the following output, when running: `docker run -it cpp-docker:0.0.1 1000 7`.
```bash
A simple Example Application
Remainint Iterations: 7	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 6	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 5	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 4	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 3	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 2	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)

Remainint Iterations: 1	 (sleep: 1000)
	(run application with: cpp-hello-world <sleepInMilliseconds> <iterations>)
```

# Kubernetes
The `deployment.yaml` contains a simple Kubernetes Pod file.

## Start Pod:
``` bash
kubectl apply -f deployment.yaml
```

### Log output of Pod
``` bash
kubectl logs --previous=false -f cpp-demo
```

## Delete Pod:
``` bash
kubectl delete -n default pod cpp-demo
```

## Persistent Volumes
### Create PV/PVC/StorageClass
``` bash
kubectl create -f simple_pv.yaml
kubectl create -f simple_pvc.yaml
kubectl create -f simple_storage_class.yaml
```

### Show PV/PVC/StorageClass
``` bash
kubectl get persistentvolume
kubectl get persistentvolumeclaim
kubectl get storageclass
```

### Delete PV/PVC/StorageClass
``` bash
kubectl delete persistentvolumeclaim simple-pvc
kubectl delete storageclass simple-storage-class
kubectl delete persistentvolume simple-pv
```
